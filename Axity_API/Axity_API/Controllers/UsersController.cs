﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Axity_API.Contexts;
using Axity_API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Axity_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public UsersController(ApplicationDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<User>> Get()
        {
            return context.Users.ToList();
        }

        [HttpGet("getUser")]
        public async Task<ActionResult> GetUser([FromBody] User value)
        {
            var user = this.context.Users.Where(a => a.Email.Equals(value.Email) && a.Password.Equals(value.Password)).FirstOrDefault();
            if (user == null)
            {
                return NotFound("Email o Password incorrecto.");
            }
            return Ok(user);
        }
    }
}
