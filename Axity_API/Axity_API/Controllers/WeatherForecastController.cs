﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Axity_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    { 

        [HttpGet]
        public ActionResult<WeatherForecast> Get()
        {
            var respuesta = "API Start";
            return Ok(respuesta);
        }
    }
}
