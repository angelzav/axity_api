﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Axity_API.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string NombreProducto { get; set; }
        public string NumCodigo { get; set; }
        public int Precio { get; set; }
        public int Stock { get; set; }
    }
}
