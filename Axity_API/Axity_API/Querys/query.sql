
CREATE DATABASE AxityDB
GO

USE AxityDB
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Products](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NombreProducto] [nvarchar](max) NULL,
	[NumCodigo] [nvarchar](max) NULL,
	[Precio] [int] NOT NULL,
	[Stock] [int] NOT NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

INSERT INTO Products (nombreProducto, numCodigo, precio,stock) VALUES ('Producto Uno','3548658435249',2800,10)
INSERT INTO Products (nombreProducto, numCodigo, precio,stock) VALUES ('Producto Dos','3549658435249',2800,25)
INSERT INTO Products (nombreProducto, numCodigo, precio,stock) VALUES ('Producto Tres','3548658985555',2800,1)
INSERT INTO Products (nombreProducto, numCodigo, precio,stock) VALUES ('Producto Cuatro','3548658436985',2800,100)
INSERT INTO Products (nombreProducto, numCodigo, precio,stock) VALUES ('Producto Cinco','1234567890123',2800,145)
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

INSERT INTO Users (email, password) VALUES ('zavala_angel@hotmail.com', 'abc123')
INSERT INTO Users (email, password) VALUES ('angel.zavala@gmail.com', 'superpass')